package homework.chegg.com.chegghomework

import homework.chegg.com.chegghomework.contract.IRemoteRepository
import homework.chegg.com.chegghomework.model.Article
import homework.chegg.com.chegghomework.model.ArticleResult
import homework.chegg.com.chegghomework.ui.ArticleIntent
import homework.chegg.com.chegghomework.ui.ArticleViewModel
import homework.chegg.com.chegghomework.ui.ArticleViewState
import io.mockk.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.*

@ExperimentalCoroutinesApi
class ViewModelTest {

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @ExperimentalCoroutinesApi
    private val testDispatcher = TestCoroutineDispatcher()

    @Before
    fun setup() {
        // 1
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun tearDown() {
        // 2
        Dispatchers.resetMain()
        // 3
        testDispatcher.cleanupTestCoroutines()
    }

    private val repo = mockk<IRemoteRepository>()
    private val vm = ArticleViewModel(repo)
    private val state = vm.viewStates()

    @Test
    fun `View Model Success Request`() = testDispatcher.runBlockingTest {
        coEvery { repo.fetchArticles() } returns ArticleResult.Data(listOf(Article("title","subTile","url")))
        vm.obtainIntent(ArticleIntent.FetchArticles)
        delay(2000)
        coVerify { repo.fetchArticles() }
        Assert.assertEquals(state.value, ArticleViewState.Show(listOf(Article("title","subTile","url"))))
    }

    @Test
    fun `View Model Error`() = testDispatcher.runBlockingTest {
        coEvery { repo.fetchArticles() } returns ArticleResult.Error("Failed To Load")
        vm.obtainIntent(ArticleIntent.FetchArticles)
        delay(2000)
        coVerify { repo.fetchArticles() }
        Assert.assertEquals(state.value,ArticleViewState.Error("Failed To Load"))
    }

    @Test
    fun `View Model States Loading`() = testDispatcher.runBlockingTest {
        coEvery { repo.fetchArticles() } returns ArticleResult.Data(listOf(Article("title","subTile","url")))
        vm.obtainIntent(ArticleIntent.FetchArticles)
        Assert.assertEquals(state.value,ArticleViewState.Loading)
        delay(1000)
    }
}