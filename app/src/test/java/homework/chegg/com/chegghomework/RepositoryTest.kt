package homework.chegg.com.chegghomework

import homework.chegg.com.chegghomework.contract.IRemoteSource
import homework.chegg.com.chegghomework.data.api.ServiceApiA
import homework.chegg.com.chegghomework.data.api.ServiceApiB
import homework.chegg.com.chegghomework.data.api.ServiceApiC
import homework.chegg.com.chegghomework.data.entity.*
import homework.chegg.com.chegghomework.data.network.RemoteSourceA
import homework.chegg.com.chegghomework.data.network.RemoteSourceB
import homework.chegg.com.chegghomework.data.network.RemoteSourceC
import homework.chegg.com.chegghomework.data.repository.RemoteRepository
import homework.chegg.com.chegghomework.model.Article
import homework.chegg.com.chegghomework.model.ArticleResult
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.*
import org.junit.*
import retrofit2.Response


@ExperimentalCoroutinesApi
class RepositoryTest {

    @ExperimentalCoroutinesApi
    private val testDispatcher = TestCoroutineDispatcher()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Before
    fun setup() {
        // 1
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun tearDown() {
        // 2
        Dispatchers.resetMain()
        // 3
        testDispatcher.cleanupTestCoroutines()
    }

    private val stores = Store("title", "subTitle", "URL")
    private val metaData =
        MetaData(listOf(InnerData(1, ArticleWrapper("description", "header"), "url")))
    private val articleC = ArticleSourceC("topLine1", "subLine", "subLine2", "url")


    @Test
    fun `repo services fetch`() = testDispatcher.runBlockingTest {
        val resourceA = mockk<IRemoteSource>()
        val resourceB = mockk<IRemoteSource>()
        val resourceC = mockk<IRemoteSource>()

        val article = Article("title", "subTile", "url")
        coEvery { resourceA.fetch() } returns ArticleResult.Data(listOf(article, article, article))
        coEvery { resourceB.fetch() } returns ArticleResult.Data(listOf(article, article, article))
        coEvery { resourceC.fetch() } returns ArticleResult.Data(listOf(article, article, article))
        val serviceList = arrayOf(resourceA, resourceB, resourceC)
        val repo = RemoteRepository(serviceList)
        val result = repo.fetchArticles() as ArticleResult.Data
        Assert.assertEquals(result.article.size, 9)


    }

    @Test
    fun `remote source A`() = testDispatcher.runBlockingTest {
        val serviceApiA = mockk<ServiceApiA>()
        coEvery { serviceApiA.fetchArticles() } returns Response.success(
            ArticleSourceA(
                listOf(
                    stores
                )
            )
        )
        val remoteSourceA = RemoteSourceA(serviceApiA)
        val result = remoteSourceA.fetch() as ArticleResult.Data
        Assert.assertEquals(result.article.size, 1)

    }

    @Test
    fun `remote source B`() = testDispatcher.runBlockingTest {
        val serviceApiB = mockk<ServiceApiB>()
        coEvery { serviceApiB.fetchArticles() } returns Response.success(
            ArticleSourceB(metaData)
        )
        val remoteSourceA = RemoteSourceB(serviceApiB)
        val result = remoteSourceA.fetch() as ArticleResult.Data
        Assert.assertEquals(result.article.size, 1)

    }

    @Test
    fun `remote source C`() = testDispatcher.runBlockingTest {
        val serviceApiC = mockk<ServiceApiC>()
        coEvery { serviceApiC.fetchArticles() } returns Response.success(listOf(articleC))
        val remoteSourceA = RemoteSourceC(serviceApiC)
        val result = remoteSourceA.fetch() as ArticleResult.Data
        Assert.assertEquals(result.article.size, 1)

    }

}