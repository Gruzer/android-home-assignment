package homework.chegg.com.chegghomework.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import homework.chegg.com.chegghomework.R
import homework.chegg.com.chegghomework.databinding.CardItemLayoutBinding
import homework.chegg.com.chegghomework.model.Article

class ArticleAdapter(private var articles: List<Article>) :
    RecyclerView.Adapter<ArticleAdapter.ArticleItemViewHolder>() {

    fun updateList(newArticle: List<Article>) {
        val diffResult = DiffUtil.calculateDiff(ArticleDiffCallBack(articles, newArticle),true)
        articles = newArticle
        diffResult.dispatchUpdatesTo(this)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleItemViewHolder {
        val binding = CardItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ArticleItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ArticleItemViewHolder, position: Int) {
        holder.bind(articles[position])
    }

    override fun getItemCount(): Int {
        return articles.size
    }


    inner class ArticleItemViewHolder(private val binding: CardItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private val image = binding.cardItemImage
        private val title = binding.cardItemTitle
        private val subTitle = binding.cardItemSubtitle
        fun bind(articles: Article) {
            title.text = articles.title
            subTitle.text = articles.subTitle
            Glide.with(image.context).load(articles.imgUrl).placeholder(R.drawable.place_holder).error(R.drawable.not_availible).into(image)
        }
    }
}

