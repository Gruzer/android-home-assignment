package homework.chegg.com.chegghomework.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import homework.chegg.com.chegghomework.contract.IRemoteRepository
import homework.chegg.com.chegghomework.contract.IRemoteSource
import homework.chegg.com.chegghomework.data.api.ServiceApiA
import homework.chegg.com.chegghomework.data.api.ServiceApiB
import homework.chegg.com.chegghomework.data.api.ServiceApiC
import homework.chegg.com.chegghomework.data.network.*
import homework.chegg.com.chegghomework.utils.Consts
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object RetrofitModule {

    @Provides
    fun provideCache(@ApplicationContext context: Context): Cache {
        val cacheSize = (5 * 1024 * 1024).toLong() //5Mb
        return Cache(context.cacheDir, cacheSize)
    }

    @SourceAClient
    @Provides
    fun provideAClient(cache: Cache): OkHttpClient {
        val clientBuilder = OkHttpClient.Builder()
        clientBuilder.cache(cache)
        clientBuilder.addNetworkInterceptor { chain ->
            val request = chain.request()
            request.newBuilder().header("Cache-Control", "public, max-age=" + 300).build()
            chain.proceed(request)
        }
        return clientBuilder.build()

    }

    @SourceBClient
    @Provides
    fun provideBClient(cache: Cache): OkHttpClient {
        val clientBuilder = OkHttpClient.Builder()
        clientBuilder.cache(cache)
        clientBuilder.addNetworkInterceptor { chain ->
            val request = chain.request()
            request.newBuilder().header("Cache-Control", "public, max-age=" + 1800).build()
            chain.proceed(request)
        }
        return clientBuilder.build()

    }

    @SourceCClient
    @Provides
    fun provideCClient(cache: Cache): OkHttpClient {
        val clientBuilder = OkHttpClient.Builder()
        clientBuilder.cache(cache)
        clientBuilder.addNetworkInterceptor { chain ->
            val request = chain.request()
            request.newBuilder().header("Cache-Control", "public, max-age=" + 3600).build()
            chain.proceed(request)
        }
        return clientBuilder.build()

    }


    @SourceAUrl
    @Provides
    fun serviceAUrl(): String {
        return Consts.DATA_SOURCE_A_URL
    }

    @SourceBUrl
    @Provides
    fun serviceBUrl(): String {
        return Consts.DATA_SOURCE_B_URL
    }

    @SourceCUrl
    @Provides
    fun serviceCUrl(): String {
        return Consts.DATA_SOURCE_C_URL
    }


    @Provides
    @Singleton
    fun provideServiceA(
        @SourceAUrl url: String,
        @SourceAClient client: OkHttpClient
    ): ServiceApiA {
        val retrofit = Retrofit.Builder()
            .baseUrl(url)
            .client(client)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
        return retrofit.create(ServiceApiA::class.java)
    }

    @Provides
    @Singleton
    fun provideServiceB(
        @SourceBUrl url: String,
        @SourceBClient client: OkHttpClient
    ): ServiceApiB {
        val retrofit = Retrofit.Builder()
            .baseUrl(url)
            .client(client)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
        return retrofit.create(ServiceApiB::class.java)
    }

    @Provides
    @Singleton
    fun provideServiceC(
        @SourceCUrl url: String,
        @SourceCClient client: OkHttpClient
    ): ServiceApiC {
        val retrofit = Retrofit.Builder()
            .baseUrl(url)
            .client(client)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
        return retrofit.create(ServiceApiC::class.java)
    }


    @Provides
    fun provideServicesList(
        serviceA: ServiceApiA,
        serviceB: ServiceApiB, serviceC: ServiceApiC
    ): Array<IRemoteSource> {
        return arrayOf(
            RemoteSourceA(serviceA),
            RemoteSourceB(serviceB),
            RemoteSourceC(serviceC)
        )

    }

}