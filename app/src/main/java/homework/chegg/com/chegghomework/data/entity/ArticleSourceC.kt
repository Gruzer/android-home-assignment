package homework.chegg.com.chegghomework.data.entity

import com.squareup.moshi.Json

data class ArticleSourceC(
    @field:Json(name = "topLine")val topLine:String,
    @field:Json(name = "subLine1")val subLine1:String,
    @field:Json(name = "subline2")val subLine2:String,
    @field:Json(name = "image")val image:String
)

