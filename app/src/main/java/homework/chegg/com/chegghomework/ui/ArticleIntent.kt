package homework.chegg.com.chegghomework.ui

sealed class ArticleIntent
{
    object FetchArticles: ArticleIntent()
}
