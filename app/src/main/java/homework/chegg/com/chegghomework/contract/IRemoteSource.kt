package homework.chegg.com.chegghomework.contract


import homework.chegg.com.chegghomework.model.ArticleResult

interface IRemoteSource {
    /**
     * Fetch articles from remote service
     */
    suspend fun fetch(): ArticleResult
}