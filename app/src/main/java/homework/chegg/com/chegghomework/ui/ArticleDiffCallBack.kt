package homework.chegg.com.chegghomework.ui

import androidx.recyclerview.widget.DiffUtil
import homework.chegg.com.chegghomework.model.Article

class ArticleDiffCallBack(
    private val oldArticles: List<Article>,
    private val newArticles: List<Article>
) : DiffUtil.Callback() {
    override fun getOldListSize(): Int {
        return oldArticles.size
    }

    override fun getNewListSize(): Int {
        return newArticles.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldArticles[oldItemPosition] == newArticles[newItemPosition]
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldArticles[oldItemPosition].title == newArticles[newItemPosition].title
    }
}