package homework.chegg.com.chegghomework.data.api


import homework.chegg.com.chegghomework.data.entity.ArticleSourceC
import retrofit2.Response
import retrofit2.http.GET

interface ServiceApiC {
    @GET("android_homework_datasourceC.json")
    suspend fun fetchArticles(): Response<List<ArticleSourceC>>

}