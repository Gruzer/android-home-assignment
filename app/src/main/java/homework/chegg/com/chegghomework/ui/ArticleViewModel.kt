package homework.chegg.com.chegghomework.ui

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import homework.chegg.com.chegghomework.contract.IRemoteRepository
import homework.chegg.com.chegghomework.model.Article
import homework.chegg.com.chegghomework.model.ArticleResult
import homework.chegg.com.chegghomework.ui.base.BaseViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ArticleViewModel
@Inject constructor(private val remoteRepository: IRemoteRepository) :
    BaseViewModel<ArticleViewState, ArticleIntent>() {

    override fun obtainIntent(userIntent: ArticleIntent) {
        when (userIntent) {
            ArticleIntent.FetchArticles -> fetchData()
        }

    }

    private fun fetchData() {
        viewModelScope.launch {
            viewState = ArticleViewState.Loading
            delay(1000) //for animation
            val result: ArticleResult = remoteRepository.fetchArticles()
            viewState = ArticleViewState.Done
            handle(result)

        }
    }

    private fun handle(result: ArticleResult) {
        when (result) {
            is ArticleResult.Data -> show(result.article)
            is ArticleResult.Error -> error(result.message)
        }
    }

    private fun error(message: String) {
        viewState = ArticleViewState.Error(message)
    }

    private fun show(articles: List<Article>) {
        viewState = ArticleViewState.Show(articles)
    }
}