package homework.chegg.com.chegghomework.contract

import homework.chegg.com.chegghomework.model.ArticleResult

interface IRemoteRepository {
    /**
     * Fetch articles from remote sources
     * @return [ArticleResult]
     */
    suspend fun fetchArticles(): ArticleResult
}