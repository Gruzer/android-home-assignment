package homework.chegg.com.chegghomework.data.api

import homework.chegg.com.chegghomework.data.entity.ArticleSourceA
import retrofit2.Response
import retrofit2.http.GET

interface ServiceApiA {

    @GET("android_homework_datasourceA.json")
    suspend fun fetchArticles():Response<ArticleSourceA>
}