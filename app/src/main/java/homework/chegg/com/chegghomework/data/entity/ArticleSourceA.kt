package homework.chegg.com.chegghomework.data.entity

import com.squareup.moshi.Json

data class ArticleSourceA(
    @field:Json(name = "stories") val stories: List<Store>
)

data class Store(
    @field:Json(name = "title")val title: String,
    @field:Json(name = "subtitle")val subTitle: String,
    @field:Json(name = "imageUrl")val imageURL: String
)
