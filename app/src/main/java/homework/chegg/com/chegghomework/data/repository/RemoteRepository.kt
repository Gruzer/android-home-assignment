package homework.chegg.com.chegghomework.data.repository

import homework.chegg.com.chegghomework.contract.IRemoteRepository
import homework.chegg.com.chegghomework.contract.IRemoteSource
import homework.chegg.com.chegghomework.model.Article
import homework.chegg.com.chegghomework.model.ArticleResult
import kotlinx.coroutines.*

class RemoteRepository(val services: Array<IRemoteSource>) : IRemoteRepository {
    override suspend fun fetchArticles(): ArticleResult = coroutineScope {
        val deferreds = mutableListOf<Deferred<ArticleResult>>()
        val list = mutableListOf<Article>()
        services.forEach { remoteSource ->
            val task = async(Dispatchers.IO) { remoteSource.fetch() }
            deferreds.add(task)
        }
        deferreds.awaitAll().map { result ->
            when (result) {
                is ArticleResult.Data -> list.addAll(result.article)
                is ArticleResult.Error -> return@coroutineScope ArticleResult.Error(result.message)
            }
        }
        return@coroutineScope ArticleResult.Data(list)
    }

}