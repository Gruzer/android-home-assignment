package homework.chegg.com.chegghomework.data.entity

import com.squareup.moshi.Json

data class ArticleSourceB(
    @field:Json(name = "metadata")val metadata: MetaData
)


data class MetaData(
    @field:Json(name = "innerdata")val innerData: List<InnerData>)

data class InnerData(
    @field:Json(name = "aticleId")val id: Int,
    @field:Json(name = "articlewrapper")val articleWrapper: ArticleWrapper,
    @field:Json(name = "picture")val picture: String
)


data class ArticleWrapper(
    @field:Json(name = "description")val description: String,
    @field:Json(name = "header")val header: String)
