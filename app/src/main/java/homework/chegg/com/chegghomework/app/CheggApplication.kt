package homework.chegg.com.chegghomework.app

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CheggApplication: Application() {
}