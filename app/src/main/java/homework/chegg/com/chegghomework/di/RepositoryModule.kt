package homework.chegg.com.chegghomework.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import homework.chegg.com.chegghomework.contract.IRemoteRepository
import homework.chegg.com.chegghomework.contract.IRemoteSource
import homework.chegg.com.chegghomework.data.repository.RemoteRepository
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Provides
    @Singleton
    fun provideRepository(serviceList:Array<IRemoteSource>):IRemoteRepository{
        return RemoteRepository(serviceList)
    }
}