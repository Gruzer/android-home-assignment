package homework.chegg.com.chegghomework.ui

import android.graphics.Rect
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dagger.hilt.android.AndroidEntryPoint
import homework.chegg.com.chegghomework.R
import homework.chegg.com.chegghomework.contract.IRemoteRepository
import homework.chegg.com.chegghomework.model.Article
import homework.chegg.com.chegghomework.model.ArticleResult
import kotlinx.coroutines.flow.collect
import javax.inject.Inject
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import homework.chegg.com.chegghomework.databinding.ActivityMainLayoutBinding


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private val articleViewModel: ArticleViewModel by viewModels()
    private val adapter: ArticleAdapter = ArticleAdapter(emptyList())
    private lateinit var binding: ActivityMainLayoutBinding
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var toolbar: Toolbar
    private lateinit var mRecyclerView: RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainLayoutBinding.inflate(layoutInflater)
        setContentView(binding.root)
        buildUI(binding)
        lifecycleScope.launchWhenResumed {
            articleViewModel.obtainIntent(ArticleIntent.FetchArticles)
            articleViewModel.viewStates().collect {
                it?.let {
                    render(it)
                }
            }
        }
    }

    private fun render(viewState: ArticleViewState) {
        when (viewState) {
            ArticleViewState.Done ->  {swipeRefreshLayout.isRefreshing = false}
            is ArticleViewState.Error -> showMessage("Error ${viewState.message}")
            ArticleViewState.Loading -> {swipeRefreshLayout.isRefreshing = true}
            ArticleViewState.NONE -> showMessage("Hello")
            is ArticleViewState.Show -> showNewArticles(viewState.article)
        }
    }

    private fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT)
            .show()
    }

    private fun showNewArticles(articles: List<Article>) {
        adapter.updateList(articles)
    }


    private fun setupToolbar(binding: ActivityMainLayoutBinding) {
        toolbar = binding.mainToolbar.root
        setSupportActionBar(toolbar)
    }

    private fun setupSwipeToRefresh(binding: ActivityMainLayoutBinding) {
        swipeRefreshLayout = binding.swiperefresh
        swipeRefreshLayout.setOnRefreshListener {
            articleViewModel.obtainIntent(ArticleIntent.FetchArticles)
            swipeRefreshLayout.isRefreshing = true
        }
    }

    private fun setupRecycleView(binding: ActivityMainLayoutBinding) {
        mRecyclerView = binding.mainRecycleView
        val linearLayoutManager = LinearLayoutManager(this)
        mRecyclerView.layoutManager = linearLayoutManager
        mRecyclerView.adapter = adapter

        mRecyclerView.addItemDecoration(object : RecyclerView.ItemDecoration() {

            override fun getItemOffsets(
                outRect: Rect,
                view: View,
                parent: RecyclerView,
                state: RecyclerView.State,
            ) {
                super.getItemOffsets(outRect, view, parent, state)
                if (parent.getChildAdapterPosition(view) > 0) {
                    outRect.top =
                        20 // Change this value with anything you want. Remember that you need to convert integers to pixels if you are working with dps :)
                }
            }
        })
    }

    private fun buildUI(binding: ActivityMainLayoutBinding) {
        setupToolbar(binding)
        setupRecycleView(binding)
        setupSwipeToRefresh(binding)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main_activity, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_refresh -> {
                onRefreshData()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    // TODO fetch data from all data sources, aggregate data and display in RecyclerView
    private fun onRefreshData() {
        articleViewModel.obtainIntent(ArticleIntent.FetchArticles)
    }
}