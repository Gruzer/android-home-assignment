package homework.chegg.com.chegghomework.data.network

import homework.chegg.com.chegghomework.contract.IRemoteSource
import homework.chegg.com.chegghomework.data.api.*
import homework.chegg.com.chegghomework.model.Article
import homework.chegg.com.chegghomework.model.ArticleResult
import homework.chegg.com.chegghomework.data.entity.ArticleSourceB

class RemoteSourceB constructor(private val serviceAPI: ServiceApiB) : IRemoteSource {
    override suspend fun fetch(): ArticleResult {
        val apiResult = serviceAPI.fetchArticles()
        return when(val result = ApiResponse.create(apiResult)){
            is ApiErrorResponse -> ArticleResult.Error("Error")
            is ApiSuccessEmptyResponse -> ArticleResult.Error("Empty response")
            is ApiSuccessResponse -> ArticleResult.Data( parse(result.data))
            else -> ArticleResult.Error("Error")
        }
    }

    private fun parse(articles: ArticleSourceB?): List<Article> {
        val list = mutableListOf<Article>()
        articles?.metadata?.innerData?.forEach {
            val imageUrl = it.picture
            val title = it.articleWrapper.header
            val subTitle = it.articleWrapper.description
            list.add(Article(title, subTitle, imageUrl))
        }
        return list
    }
}