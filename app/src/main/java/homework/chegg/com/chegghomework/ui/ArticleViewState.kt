package homework.chegg.com.chegghomework.ui

import homework.chegg.com.chegghomework.model.Article

sealed class ArticleViewState {
    object Loading : ArticleViewState()
    object Done : ArticleViewState()
    object NONE:ArticleViewState()
    data class Show(val article: List<Article>) : ArticleViewState()
    data class Error(val message: String) : ArticleViewState()
}