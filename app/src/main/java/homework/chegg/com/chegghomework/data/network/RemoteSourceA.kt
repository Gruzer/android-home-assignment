package homework.chegg.com.chegghomework.data.network

import homework.chegg.com.chegghomework.contract.IRemoteSource
import homework.chegg.com.chegghomework.data.api.*
import homework.chegg.com.chegghomework.model.Article
import homework.chegg.com.chegghomework.model.ArticleResult
import homework.chegg.com.chegghomework.data.entity.ArticleSourceA

class RemoteSourceA constructor(private  val serviceAPI: ServiceApiA) : IRemoteSource {
    override suspend fun fetch(): ArticleResult {
        val apiResult = serviceAPI.fetchArticles()
        return when(val result = ApiResponse.create(apiResult)){
            is ApiErrorResponse -> ArticleResult.Error("Error")
            is ApiSuccessEmptyResponse -> ArticleResult.Error("Empty response")
            is ApiSuccessResponse -> ArticleResult.Data( parse(result.data))
            else -> ArticleResult.Error("Error")
        }
    }

    private  fun parse(articles: ArticleSourceA?): List<Article> {
        val list = mutableListOf<Article>()
        articles?.stories?.forEach {
            val title = it.title
            val subTitle = it.subTitle
            val url = it.imageURL
            list.add(Article(title, subTitle, url))
        }
        return list
    }
}