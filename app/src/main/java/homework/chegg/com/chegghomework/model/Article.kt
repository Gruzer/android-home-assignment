package homework.chegg.com.chegghomework.model

data class Article(
    val title: String,
    val subTitle: String,
    val imgUrl: String
)
