package homework.chegg.com.chegghomework.data.api

import homework.chegg.com.chegghomework.data.entity.ArticleSourceB
import retrofit2.Response
import retrofit2.http.GET

interface ServiceApiB {
    @GET("android_homework_datasourceB.json")
    suspend fun fetchArticles(): Response<ArticleSourceB>
}