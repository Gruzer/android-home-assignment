package homework.chegg.com.chegghomework.data.network

import homework.chegg.com.chegghomework.contract.IRemoteSource
import homework.chegg.com.chegghomework.data.api.*
import homework.chegg.com.chegghomework.model.Article
import homework.chegg.com.chegghomework.model.ArticleResult
import homework.chegg.com.chegghomework.data.entity.ArticleSourceC

class RemoteSourceC constructor(private val serviceAPI: ServiceApiC):IRemoteSource {
    override suspend fun fetch(): ArticleResult {
        val apiResult = serviceAPI.fetchArticles()
        return when(val result = ApiResponse.create(apiResult)){
            is ApiErrorResponse -> ArticleResult.Error("Error")
            is ApiSuccessEmptyResponse -> ArticleResult.Error("Empty response")
            is ApiSuccessResponse -> ArticleResult.Data( parse(result.data))
            else -> ArticleResult.Error("Error")
        }
    }

    private fun parse(articles: List<ArticleSourceC?>): List<Article> {
        val list = mutableListOf<Article>()
        articles.forEach { source->
            source?.let {
                val imageUrl = it.image
                val title = it.topLine
                val subTitle = "${it.subLine1} ${it.subLine2}"
                list.add(Article(title,subTitle,imageUrl))
            }

        }
        return list
    }
}