package homework.chegg.com.chegghomework.contract

import homework.chegg.com.chegghomework.model.ArticleResult

interface IInteractor {
    fun fetchArticles():ArticleResult
}