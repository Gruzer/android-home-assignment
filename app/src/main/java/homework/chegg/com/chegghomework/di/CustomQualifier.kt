package homework.chegg.com.chegghomework.di

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class SourceAClient


@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class SourceAUrl




@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class SourceBClient


@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class SourceBUrl


@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class SourceCClient


@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class SourceCUrl