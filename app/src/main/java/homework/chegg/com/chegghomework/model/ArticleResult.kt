package homework.chegg.com.chegghomework.model

sealed class ArticleResult {
    data class Data(val article: List<Article>) : ArticleResult()
    data class Error(val message: String) : ArticleResult()
}
